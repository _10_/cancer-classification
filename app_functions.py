import pandas as pd
import numpy as np  
from sklearn import preprocessing
from sklearn.preprocessing import LabelEncoder  
from keras.models import Sequential
from keras.layers import Dense, Dropout 
import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt  
from sklearn.model_selection import train_test_split 
from sklearn.preprocessing import LabelBinarizer
from keras import optimizers 
from sklearn.preprocessing import StandardScaler 
from keras import regularizers

def build_model():
    model = Sequential()
    model.add(Dense(8, activation='relu', input_shape=(10,))) 
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='sgd', metrics=['accuracy'])  
    
    return model

def model(X_test):
    elem =   ['Q19/Q2','Q20/Q2','Q21/Q2','Q13/Q2','Q9/Q6','Q12/Q6','Q18/Q6','Q10/Q6','Q8/Q3','Q14/Q3']
    nda = np.array(X_test)
    nda1 = np.array(X_test.values[:,  0:10 ])

    df_frame = pd.DataFrame(nda, columns = elem)   
        
    predicts_c = pc(nda1)
    nda2 = predicts_c[0]
    nda3 = predicts_c[1]
    df_frame['y'] = pd.Series(nda2, index=df_frame.index)
    df_frame.loc[df_frame.y == 0,'c/h'] = 'ill'  
    df_frame.loc[df_frame.y == 1,'c/h'] = 'healthy'  
    
    df_frame['probability'] = pd.Series(nda3, index=df_frame.index)
    
    df_frame = df_frame[['c/h', 'probability','Q19/Q2','Q20/Q2', 'Q21/Q2','Q13/Q2','Q9/Q6','Q12/Q6','Q18/Q6','Q10/Q6','Q8/Q3','Q14/Q3']]
    return df_frame
    
def pc(xtest):    
    model = build_model()
    model.load_weights('weight_norm_5')
 
    if xtest.size / 2 == 1:
        xtest = np.reshape(xtest, ( 1, -1)) 
   
    predict_2 =  model.predict_classes(xtest, batch_size=5) 
    
    predict_proba = model.predict_proba(xtest)
    
    predict_3 = []
    for i in predict_2:
        predict_3.append(i[0])
    
    probability = []
    for i in predict_proba:
        if i[0] < 0.5:
            i[0] = 1 - i[0]
        probability.append(str(round(i[0] * 100, 2) ) + '%')

    return [predict_3, probability]
 
def predict_classes(df):    
    df_frame = model(df)
    return df_frame